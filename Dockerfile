FROM ubuntu:16.04
MAINTAINER Adi Djohari <adidjohari@gmail.com>

# Let's Build
# Asks apt-get to skip any interactive post-install configuration steps
ENV DEBIAN_FRONTEND noninteractive

# Sets local settings
ENV LC_ALL en_US.UTF-8
ENV LANGUAGE en_US:en

# Base
RUN \
  apt-get update && \
  apt-get -y upgrade && \
  apt-get install -y build-essential sudo && \
  apt-get -y --no-install-recommends install locales apt-utils apt-transport-https curl gnupg ca-certificates && \
  echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen && \
  locale-gen en_US.UTF-8 && \
  /usr/sbin/update-locale LANG=en_US.UTF-8 && \
  update-ca-certificates && \
  apt-get autoclean && apt-get clean && apt-get autoremove

# Install deps + add Chrome Stable + purge all the things
RUN apt-get update \
	&& apt-get install -y \
	libzip-dev libxpm4 libxrender1 libgtk2.0-0 libnss3 libgconf-2-4 \
	xvfb gtk2-engines-pixbuf xfonts-cyrillic xfonts-100dpi xfonts-75dpi \
	xfonts-base xfonts-scalable imagemagick x11-apps \
	&& curl -sSL https://dl.google.com/linux/linux_signing_key.pub | apt-key add - \
	&& echo "deb [arch=amd64] https://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list \
	&& apt-get update && apt-get install -y \
	google-chrome-stable \
	--no-install-recommends \
	&& apt-get clean \
    && apt-get autoremove\
	&& rm -rf /var/lib/apt/lists/*

# Grab the requirements of Composer
RUN apt-get update \
    && apt-get install -y \
    libfreetype6 \
    libfreetype6-dev \
    libssl-dev \
    libmcrypt-dev \
    libpng12-dev \
    libbz2-dev \
    libxslt-dev \
    libxft-dev \
    libldap2-dev \
    libtidy-dev \
    libfontconfig1 \
    libfontconfig1-dev \
    php-pear \
    git \
    unzip \
    make \
    gcc \
    wget \
    python-software-properties \
    && apt-get clean \
    && apt-get autoremove\
    && rm -r /var/lib/apt/lists/*

# Install PHP 7.1
RUN apt-get update && \
  apt-get install -y software-properties-common && \
  add-apt-repository ppa:ondrej/php && \
  apt-get update && \
  apt-get install -y \
  git zip && \
  apt-get install -y php7.1-mysqlnd php7.1-cli php7.1-common php7.1-sqlite php7.1-mbstring php7.1-mcrypt \
  php7.1-curl php7.1-gd php7.1-zip php7.1-xml php7.1-json php7.1-opcache php7.1-mysql \
  php7.1-phpdbg php7.1-imap php7.1-ldap php7.1-pspell php7.1-recode php7.1-tidy php7.1-dev \
  php7.1-intl php7.1-zip php7.1-xmlrpc php7.1-redis php7.1-bz2 php7.1-bcmath && \
  apt-get autoclean && apt-get clean && apt-get autoremove

# Add our PHP settings file
ADD php-tools.ini /etc/php/7.1/cli/conf.d/php-tools.ini

# Test and check PHP
RUN php -v

# add our user and group first to make sure their IDs get assigned consistently, regardless of whatever dependencies get added
RUN groupadd -r mysql && useradd -r -g mysql mysql

RUN apt-get update && \
  apt-get install -y mysql-server && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/*

# comment out a few problematic configuration values
# don't reverse lookup hostnames, they are usually another container
RUN sed -Ei 's/^(bind-address|log|skip-networking)/#&/' /etc/mysql/mysql.conf.d/mysqld.cnf \
  && echo '[mysqld]\nskip-host-cache\nskip-name-resolve\n[client]\nport=3306\nsocket=/var/run/mysqld/mysqld.sock' > /etc/mysql/conf.d/docker.cnf

RUN mkdir -p /var/lib/mysql /var/run/mysqld && \
  chown -R mysql:mysql /var/lib/mysql /var/run/mysqld && \
  echo "mysqld_safe &" > /tmp/config && \
  echo "mysqladmin --silent --wait=30 ping || exit 1" >> /tmp/config && \
  echo "mysql -e 'ALTER USER \"root\"@\"localhost\" IDENTIFIED BY \"root\";'" >> /tmp/config && \
  bash /tmp/config && \
  rm -f /tmp/config

# Define mountable directories.
VOLUME ["/etc/mysql", "/var/lib/mysql"]

# Install composer, including signature check
RUN curl -sS https://getcomposer.org/installer | php -- --filename=composer --install-dir=/usr/bin

# Install Node+NPM
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -

RUN apt-get update && \
  apt-get install -y nodejs && \
    apt-get autoclean && apt-get clean && apt-get autoremove

RUN node -v

RUN npm -v

RUN npm install -g karma-cli
RUN npm install -g bower
RUN npm install -g gulp-cli

# Add birdsend as a user
RUN groupadd -r birdsend && useradd -r -g birdsend -G audio,video birdsend \
    && mkdir -p /home/birdsend && chown -R birdsend:birdsend /home/birdsend

# Run birdsend non-privileged
USER birdsend

# Define working directory.
WORKDIR /app

# Default action is to run PHP interactively
CMD ["php", "-a"]